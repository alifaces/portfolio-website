import LineGradient from "../components/LineGradient";
import { motion } from "framer-motion";

const container = {
  hidden: {},
  visible: {
    transition: {
      staggerChildren: 0.2,
    },
  },
};

const projectVariant = {
  hidden: { opacity: 0, scale: 0.8 },
  visible: { opacity: 1, scale: 1 },
};

const Project = ({ title, description, url }) => {
  const overlayStyles = `absolute h-full w-full opacity-0 hover:opacity-90 transition duration-500 bg-grey z-30 flex flex-col justify-center items-center text-center p-16 text-deep-blue`;
  const projectTitle = title.split(" ").join("-").toLowerCase();

  return (
    // Ensure the <a> tag uses the `url` prop correctly and opens the link in a new tab securely
    <a href={url} target="_blank" rel="noopener noreferrer" className="m-4 block">
      <motion.div variants={projectVariant} className="relative group cursor-pointer">
        <div className={overlayStyles}>
          <p className="text-2xl font-playfair">{title}</p>
          <p className="mt-7">{description}</p>
        </div>
        <img src={`../assets/${projectTitle}.png`} alt={projectTitle} className="w-full h-auto" />
      </motion.div>
    </a>
  );
};


const Projects = () => {
  return (
    <section id="projects" className="pt-48 pb-48">
      {/* HEADINGS */}
      <motion.div
        className="md:w-2/5 mx-auto text-center"
        initial="hidden"
        whileInView="visible"
        viewport={{ once: true, amount: 0.5 }}
        transition={{ duration: 0.5 }}
        variants={{
          hidden: { opacity: 0, y: -50 },
          visible: { opacity: 1, y: 0 },
        }}
      >
        <div>
          <p className="font-playfair font-semibold text-4xl">
            <span className="text-white">PRO</span>JECTS
          </p>
          <div className="flex justify-center mt-5">
            <LineGradient width="w-2/3" />
          </div>
        </div>
        <p className="mt-10 mb-10">
        Explore Recent Projects: Tap on an image to visit the website, or hover for details.
        </p>
      </motion.div>

      {/* PROJECTS */}
      <div className="flex justify-center">
        <motion.div
          className="sm:grid sm:grid-cols-3 gap-4"
          variants={container}
          initial="hidden"
          whileInView="visible"
          viewport={{ once: true, amount: 0.2 }}
        >
          {/* ROW 1 */}
          <Project title="Project 1"
          description="A website for a construction business." url="https://herlindo-website.pages.dev/"/>
          <Project title="Project 2"
          description="A website for a construction business." url="https://herlindo-website.pages.dev/" />
          <Project title="Project 3" description="A webite for an international conference in Florence, Italy." url="https://2024.apsursi.org/" />
          {/* ROW 2 */}

          <div className="sm:col-start-2"> {/* This will start the single project in the second column on small screens */}
            <Project title="Project 4" description="A webite for an international conference in Athens, Greece" url="https://www.2024.ieeeigarss.org/"/>
          </div>
          {/* <Project title="Project 5" /> */}

        </motion.div>
      </div>
    </section>
  );
};

export default Projects;
