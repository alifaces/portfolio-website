import { motion } from "framer-motion";
import LineGradient from "../components/LineGradient";
import backgroundImage from '../assets/person-1.jpeg'; // Ensure this path is correct

const MySkills = () => {
  return (
    <section id="skills"
      style={{
        backgroundImage: `url(${backgroundImage})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center center',
        backgroundRepeat: 'no-repeat',
      }}
      className="text-white py-10 md:py-20"
      >
      <div className="container mx-auto px-10 lg:px-10 bg-gray-900 bg-opacity-75 rounded-lg" style={{ maxWidth: 'none' }}>
        <motion.div
          initial={{ opacity: 0 }}
          animate={{ opacity: 1 }}
          transition={{ duration: 1 }}
          className="mb-12"
        >
          <h1 className="text-4xl font-bold mb-4">Technical Skills & Professional Experience</h1>
          <LineGradient width="w-1/4" />
          <p className="mt-4 text-xl">Dedicated to building immersive digital experiences with a focus on the future.</p>
        </motion.div>

        <div className="grid md:grid-cols-1 gap-6">
        <motion.div
          initial={{ x: -50, opacity: 0 }}
          animate={{ x: 0, opacity: 1 }}
          transition={{ delay: 0.2, duration: 0.5 }}
          className="min-h-[150px]" // Reduced padding, added a minimum height
        >
          <h2 className="text-2xl font-semibold mb-4">Professional Roles</h2>
          <ul className="list-disc pl-4"> {/* Adjusted padding to match box padding */}
            <li>Full-stack Developer at Conference Management Services, Inc. - Spearheaded web interface development for IEEE international conferences.</li>
            <li>Software Engineer at GemStack - Enhanced data analytics and automated processes using Python and Golang.</li>
          </ul>
        </motion.div>
        <motion.div
          initial={{ x: -50, opacity: 0 }}
          animate={{ x: 0, opacity: 1 }}
          transition={{ delay: 0.2, duration: 0.5 }}
          className="min-h-[150px]" // Applied the same adjustments here
        >
          <h2 className="text-2xl font-semibold mb-4">Technical Skills</h2>
          <ul className="list-disc pl-4"> {/* Consistent padding with the box */}
            <li>Proficient in: PHP, Python, JavaScript (ES6+), CSS, HTML, Tailwind, Bootstrap..</li>
            <li>Back End/System Design: React-Redux, Django, AWS, MongoDB, SQL, APIs, Microservices, Docker, Git</li>
          </ul>
        </motion.div>
      </div>


        {/* Projects */}
        <div className="mt-16">
          <h2 className="text-2xl font-semibold mb-4">Academic Credentials & Previous Professional Background</h2>
          <LineGradient width="w-1/4" />
          <div className="flex flex-wrap justify-start gap-8 mt-8">
            <ProjectCard
              title="Hack Reactor at Galvanize"
              description="Earned a Software Engineering Immersive Certificate, culminating in over 1,000 hours of hands-on development experience with a focus on JavaScript, Python, and full-stack technologies."
            />
            <ProjectCard
              title="Lone Star College System"
              description="Achieved an Associate of Applied Science degree, laying a solid foundation in key scientific principles and technical skills."
            />
            <ProjectCard
              title="Industry Experience in Oil & Gas"
              description="Accrued six years of valuable experience as a Pipe Welder within Houston's Oil & Gas fabrication shops, honing my technical skills and industry knowledge."
            />
          </div>
        </div>
      </div>
    </section>
  );
};

const ProjectCard = ({ title, description }) => (
  <motion.div
    initial={{ opacity: 0, scale: 0.9 }}
    animate={{ opacity: 1, scale: 1 }}
    transition={{ duration: 0.5 }}
    className="max-w-sm w-full bg-gray-800 rounded-lg shadow-lg p-4"
  >
    <h3 className="text-xl font-semibold">{title}</h3>
    <p className="mt-2">{description}</p>
  </motion.div>
);

export default MySkills;
