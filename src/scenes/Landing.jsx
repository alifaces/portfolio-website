import React from 'react';
import SocialMediaIcons from "../components/SocialMediaIcons";
import { motion } from "framer-motion";
import AnchorLink from "react-anchor-link-smooth-scroll";
import backgroundVideo from '../assets/person-4.mov';
const Landing = ({ setSelectedPage }) => {
    return (
        <section id="home" className="relative overflow-hidden w-full min-h-screen flex items-center justify-center py-10">
            <video autoPlay loop muted playsInline className="absolute z-0 w-auto min-w-full min-h-full max-w-none">
                <source src={backgroundVideo} type="video/mp4" />
                Your browser does not support the video tag.
            </video>
            <div className="z-30 w-full md:w-4/5 lg:w-3/5"> {/* Adjust width as per your design */}
                <motion.div
                    initial="hidden"
                    whileInView="visible"
                    viewport={{ once: true, amount: 0.5 }}
                    transition={{ duration: 0.5 }}
                    variants={{
                        hidden: { opacity: 0, x: -50 },
                        visible: { opacity: 1, x: 0 },
                    }}
                >
                    <p className="text-6xl font-playfair text-center md:text-left">
                        Alexis Hernandez
                    </p>

                    {/* <p className="mt-10 mb-0 text-md text-center md:text-left">
                        Software Engineer Developer, with an eye for data driven design and analysis. */}
                        {/* <div className="flex justify-center md:justify-start mt-5">
                            <a href="mailto:alexhh713@gmail.com" className="text-white hover:underline">alexhh713@gmail.com</a>
                        </div> */}
                    {/* </p> */}
                    {/* Your description paragraphs */}
                    <div className="mt-5 text-center md:text-left space-y-4">
  <h2 className="text-xl md:text-2xl font-semibold text-white">
    Elevate Your Digital Presence
  </h2>
  <div className="text-md text-gray-300">
    <p>I'm a versatile <span className="text-cyan-400 font-semibold">Full-stack Developer</span> with a knack for transforming ideas into dynamic web and app solutions.</p>
    {/* <p>My toolkit includes <span className="font-semibold">PHP, Python, JavaScript, SQL</span>, and cutting-edge technologies like <span className="text-blue-400 font-semibold">React</span> and <span className="text-blue-400 font-semibold">AWS</span>.</p> */}
  </div>
  <div className="pt-2">
    <span className="inline-block bg-gray rounded-full px-3 py-1 text-sm font-semibold text-white mr-2 mb-2">Web Development</span>
    <span className="inline-block bg-gray rounded-full px-3 py-1 text-sm font-semibold text-white mr-2 mb-2">App Solutions</span>
    <span className="inline-block bg-gray rounded-full px-3 py-1 text-sm font-semibold text-white">System Design</span>
  </div>
  <p>Ready to <span className="font-semibold text-yellow-400">innovate</span> your next project?</p>

</div>
<motion.div
                        className="flex justify-center md:justify-start items-center"
                        initial="hidden"
                        whileInView="visible"
                        viewport={{ once: true, amount: 0.5 }}
                        transition={{ delay: 0.2, duration: 0.5 }}
                        variants={{
                            hidden: { opacity: 0, x: -50 },
                            visible: { opacity: 1, x: 0 },
                        }}
                    >
                        <AnchorLink
                            className="bg-gradient-rainblue text-white rounded-sm py-2 px-7 font-semibold hover:bg-blue hover:text-white transition duration-500 mr-4"
                            onClick={() => setSelectedPage("contact")}
                            href="#contact"
                        >
                            Contact Me
                        </AnchorLink>
                        <SocialMediaIcons />
                    </motion.div>
                </motion.div>

                <motion.div
                    initial="hidden"
                    whileInView="visible"
                    viewport={{ once: true, amount: 0.5 }}
                    transition={{ duration: 0.5 }}
                    variants={{
                        hidden: { opacity: 0, x: 50 },
                        visible: { opacity: 1, x: 0 },
                    }}
                >

                    {/* Repeat for other paragraphs */}
                </motion.div>
            </div>
        </section>
    );
};

export default Landing;
